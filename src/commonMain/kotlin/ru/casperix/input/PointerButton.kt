package ru.casperix.input

data class PointerButton(val code: Int) : Button {
    companion object {
        val LEFT = PointerButton(0)
        val MIDDLE = PointerButton(2)
        val RIGHT = PointerButton(1)
    }
}

